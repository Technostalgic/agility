var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");
var width = canvas.width;
var height = canvas.height;
var lastTime = 0;
var dt = 0;
//gamevars:
var p1;
var score = 0;
var _cscore = score;
var reward = {};
var port1 = null;
var port2 = null;
var enemies = [];
var gibs = [];
var arrows = [];
var geometries = [];
var controlsPressed = {};
var controlsClicked = {};
var controls = {};
var gravity = .5;
var worldLeft = 2;
var worldRight = width - 2;
var worldTop = 2;
var worldBottom = height - 2;
var worldMid = new vec2((worldLeft + worldRight) / 2, (worldTop + worldBottom) / 2);
var lives = 1;

function keyDown(args){
  //console.log(args.key + ":" + args.keyCode);
  switch(args.keyCode){
    case controls.right:
      controlsPressed.right = true;
      break;
    case controls.left: 
      controlsPressed.left = true;
      break;
    case controls.up: 
      controlsPressed.up = true;
      break;
    case controls.down: 
      controlsPressed.down = true;
      break;
    case controls.jump: 
      controlsPressed.jump = true;
      break;
    case controls.shoot: 
      controlsPressed.shoot = true;
      break;
    case controls.melee: 
      controlsPressed.melee = true;
      break;
  }
}
function keyUp(args){
  switch(args.keyCode){
    case controls.right:
      controlsPressed.right = false;
      break;
    case controls.left: 
      controlsPressed.left = false;
      break;
    case controls.up: 
      controlsPressed.up = false;
      break;
    case controls.down: 
      controlsPressed.down = false;
      break;
    case controls.jump: 
      controlsPressed.jump = false;
      break;
    case controls.shoot: 
      controlsPressed.shoot = false;
      break;
    case controls.melee: 
      controlsPressed.melee = false;
      break;
  }
}

function clrCanvas(){
  context.fillStyle = "#555";
  context.fillRect(0, 0, width, height);
}
function init(){
  document.addEventListener("keydown", keyDown);
  document.addEventListener("keyup", keyUp);
  //console.clear();
  clrCanvas();
  requestAnimationFrame(step);
  loadGame();
}
function loadGame(){
  geometry.init();
  spawnController.init();
  levelBuilder.init();
  setDefaultControls();
  p1 = new player(worldMid.copy());
  destroyPortals();
  score = 0;
  _cscore = score;
  reward = {};
  lives = 0;
  gravity = .5;

  arrows = [];
  enemies = [];
  gibs = [];
  loadLevel();
}
function step(){
  update(dt / 16.6666);
  requestAnimationFrame(step);
  dt = performance.now() - lastTime;
  lastTime = performance.now();
}
function update(timescale){
  //console.log(timescale);
  if(timescale > 5)
    timescale = 0;
  spawnController.update(timescale);
  messageController.update(timescale);
  updateEnemies(timescale);
  updateGibs(timescale);
  updatePlayer(timescale);
  updateArrows(timescale);
  draw(context);
}
function draw(context2d){
  clrCanvas();
  drawEnemies_Outline(context2d);
  drawGibs_Outline(context2d);
  drawGibs_Fill(context2d);
  drawArrows(context2d);
  drawEnemies_Fill(context2d);
  drawPlayer(context2d);
  drawGeometries(context2d);
  drawPortals(context2d);
  messageController.draw(context2d);
  drawHUD(context2d);
}

function loadLevel(){
  geometries = [];
  geometries.push(new geometry(100, 300, 100, 100));
  geometries.push(new geometry(200, 380, 250, 20));
  geometries.push(new geometry(500, 120, 20, 200));

  enemies = [];
  //enemies.push(new enemy(new vec2(300, 300), 40));
  //enemies.push(new enemy(new vec2(200, 200), 30));
  //enemies.push(new enemy(new vec2(400, 200), 30));
  //enemies.push(new enemy(new vec2(300, 100), 50));
  levelBuilder.transition();
}
function setDefaultControls(){
  controlsPressed = {
    right: false,
    left: false,
    up: false,
    down: false,
    jump: false,
    shoot: false,
    melee: false
  }
  controls = {
    right: 39,
    left: 37,
    up: 38,
    down: 40,
    jump: 90,
    shoot: 67,
    melee: 88
  };
}

function updatePlayer(ts){
  p1.update(ts);
}
function drawPlayer(ctx){
  p1.draw(ctx);
}
function drawHUD(ctx){
  drawScore(ctx);
  drawAmmoLives(ctx);
}
function drawPortals(ctx){
  if(port1)
    port1.draw(ctx);
  if(port2)
    port2.draw(ctx);
}
function drawScore(ctx){
  if(_cscore != score)
    _cscore += 10;
  if(_cscore > score)
    _cscore = score;
  ctx.fillStyle = "#fff";
  ctx.font = "30px sans-serif";
  ctx.textAlign = "center";
  ctx.fillText( _cscore.toString(), worldMid.x, 30);
}
function drawAmmoLives(ctx){
  var a = new arrow(new vec2(worldLeft + 20, worldTop + 20), -Math.PI / 4, 1);
  a.body.setScale(1.5);
  a.body.drawFill(ctx, "#ddd");
  a.body.drawOutline(ctx, "#222", 3);
  ctx.fillStyle = "#fff";
  ctx.font = "30px sans-serif";
  ctx.textAlign = "left";
  ctx.fillText("x " + p1.ammo.toString(), a.pos.x + 15, a.pos.y + 15);

  //var b = new box(worldLeft + 120, worldTop + 12, 20, 40);
  var b = new box(worldLeft + 10, worldTop + 45, 15, 30);
  b.drawFill(ctx, "#66d");
  b.drawOutline(ctx, "#113", 3);
  ctx.fillStyle = "#fff";
  ctx.fillText("x " + lives.toString(), b.position.x + 25, b.position.y + 25);
}
function updateEnemies(ts){
  for(var i = 0; i < enemies.length; i++)
    enemies[i].update(ts);
}
function drawEnemies_Fill(ctx){
  for(var i = 0; i < enemies.length; i++)
    enemies[i].drawFill(ctx);
}
function drawEnemies_Outline(ctx){
  for(var i = 0; i < enemies.length; i++)
    enemies[i].drawOutline(ctx);
}
function updateGibs(ts){
  for(var i = 0; i < gibs.length; i++)
    gibs[i].update(ts, i);
}
function drawGibs_Fill(ctx){
  for(var i = 0; i < gibs.length; i++)
    gibs[i].drawFill(ctx);
}
function drawGibs_Outline(ctx){
  for(var i = 0; i < gibs.length; i++)
    gibs[i].drawOutline(ctx);
}
function updateArrows(ts){
  for(var i = 0; i < arrows.length; i++)
    arrows[i].update(ts);
}
function drawArrows(ctx){
  for(var i = 0; i < arrows.length; i++)
    arrows[i].draw(ctx);
}
function drawGeometries(ctx){
  for(var i = 0; i < geometries.length; i++)
    geometries[i].drawOutline(ctx);
  drawBorder(ctx);
  for(var i = 0; i < geometries.length; i++)
    geometries[i].drawFill(ctx);
}
function drawBorder(ctx){
  box.fromBounds(worldLeft-2, worldRight+2, worldTop-2, worldBottom+2).drawOutline(ctx, "#000", 2);
}

function destroyPortals(){
  p1._isTeleporting = 0;
  if(port1){
    port1.num = 1;
    port1.destroy();
  }
  if(port2){
    port2.num = 2;
    port2.destroy();
  }
}
function addScore(pts = 0){
  score += pts;
  rewardScore();
}
function rewardScore(){
  if(reward.nextLife){
    if(score >= reward.nextLife)
      rewardLife();
  }
  else reward.nextLife = 1000;

  if(reward.nextArrow){
    if(score >= reward.nextArrow){
      rewardArrow();
    }
  }
  else reward.nextArrow = 250;
}
function rewardLife(){
  reward.nextLife *= 2;
  lives += 1;
  messageController.addMessage(new message("EXTRA LIFE!", p1.pos.copy().minus(new vec2(0, 30)), 180).bonusFormat());
}
function rewardArrow(){
  p1.ammo += 1;
  if(reward.nextArrowInt)
    reward.nextArrowInt += 500;
  else reward.nextArrowInt = 500;
  reward.nextArrow += reward.nextArrowInt;
  messageController.addMessage(new message("EXTRA ARROW!", p1.pos.copy().minus(new vec2(0, 30)), 180).bonusFormat());
}
function gameOver(){

}


class geometry{
  static init(){
    //set static fields
    geometry.right = 1;
    geometry.left = 2;
    geometry.top = 3;
    geometry.bottom = 4;
  }
  constructor(x = 0, y = 0, w = 1, h = 1){
    this.col = new box(x, y, w, h);
    this.isSolid = true;
  }
  fromBounds(l,t,r,b){
    return new geometry(l, t, r - l, b - t);
  }

  toConstruct(manditory = true, active = true){
    return {
      required: manditory,
      enabled: active,
      geom: this
    };
  }
  testCollision(bx){
    if(!box.testOverlap(this.col, bx))
      return 0;
    var intersect = box.intersection(this.col, bx);
    if(intersect.size.x >= intersect.size.y){
      //horizontal collision
      if(intersect.center().y >= bx.center().y)
        return geometry.top;
      return geometry.bottom;
    }
    //vertical collision
    if(intersect.center().x >= bx.center().x)
      return geometry.left;
    return geometry.right;
  }

  drawOutline(ctx){
    this.col.position.x -= 1;
    this.col.position.y -= 1;
    this.col.size.x += 2;
    this.col.size.y += 2;
    this.col.drawOutline(ctx, "#000", 2);
    this.col.position.x += 1;
    this.col.position.y += 1;
    this.col.size.x -= 2;
    this.col.size.y -= 2;
  }
  drawFill(ctx){
    this.col.drawFill(ctx, "#aaa");
  }
}
class thing{
  constructor(pos = new vec2()){
    this.pos = pos;
    this.vel = new vec2();
    this.tD = 1;
  }

  getRelTimescale(ts){
    return this.tD * ts;
  }

  update(ts){
    this.pos = this.pos.plus(this.vel.multiply(this.getRelTimescale(ts)));
  }
  draw(ctx){
  }
}
class arrow extends thing{
  constructor(pos, ang, pow){
    super(pos);
    this.vel = vec2.fromAng(ang, pow);
    this._ovel = this.vel.copy();
    this.ang = ang;
    this.bounceCoeff = 0.5;
    this.bounceFriction = 0.7;
    this.ricochetCutoff = 1;
    this.active = true;
    this._stucktime = 0;
    this._stuckAng = 0;
    this._stuckto;
    this._stuckOffset;
    this._didCollide = false;
    this._istracer = false;
    this.loadPointStats();
    this.loadBody();
    this.body.setPosition(this.pos);
    this.body.setRotation(ang);
  }
  loadBody(){
    this.body = new polygon();
    this.body.setVerts([
      new vec2(0, -1), 
      new vec2(-1, -2), 
      new vec2(-7, -1), 
      new vec2(-2, -1), 
      new vec2(-1, 0), 
      new vec2(-2, 1), 
      new vec2(-7, 1), 
      new vec2(-1, 2), 
      new vec2(0, 1), 
      new vec2(5, 0)
    ]);
    this.body.transform(new vec2(), 0, 2);
  }
  loadPointStats(chaincontinue = 0){
    this.pointStats = {
      bankshot: 0,
      skipshot: 0,
      dropshot: 0,
      lobshot: 0,
      stalactite: 0,
      chain: chaincontinue,
      snipe: 0
    };
  }

  stick(time = Infinity, obj = null){
    this.active = false;
    this.loadPointStats();
    this._stucktime = time;
    this._stuckAng = this.body.getRotation();
    this.vel = new vec2();
    this._stuckto = obj;
    if(!obj)
      return;
    if(!obj.body)
      this._stuckOffset = obj.pos - this.pos;
    else{
      this._stuckOffset = obj.body.worldPointToLocal(this.pos);
      this._stuckAng = this._stuckAng - obj.body.getRotation();
    }
  }
  beStuck(ts){
    this.vel = new vec2();
    this._stucktime -= ts;
    if(this._stucktime <= 0)
      this.unstick();
    if(!this._stuckto)
      return;
    if(!this._stuckto.body)
      this.pos = this._stuckto.pos.plus(this._stuckOffset);
    else{
      this.pos = this._stuckto.body.transformPoint(this._stuckOffset);
      this.body.setRotation(this._stuckAng + this._stuckto.body.getRotation());
    }
  }
  unstick(rebound = 2){
    this.active = true;
    this._stucktime = 0;
    if(this._stuckto instanceof enemy){
      this._stuckto.punctures.splice(this._stuckto.punctures.indexOf(this), 1);
      this.vel = vec2.fromAng(this.body.getRotation(), -5);
      this.vel = this.vel.plus(this._stuckto.vel);
    }
    else
      this.vel = vec2.fromAng(this.body.getRotation(), -rebound);
  }
  applyPhysics(ts){
    var friction = 0.99;
    this.vel = this.vel.multiply(((friction - 1) * ts) + 1);
    this.vel.y += gravity * ts;
  }
  checkPortals(ts){
    if(!(port1 && port2))
      return false;

    var projectionRay = ray.fromPoints(this.pos, this.pos.plus(this.vel.multiply(ts)));
    var int1 = port1 != null ? port1.col.rayIntersect(projectionRay) : null;
    var int2 = port2 != null ? port2.col.rayIntersect(projectionRay) : null;

    if(int1){
      this.pos = port1.transformPos(this.pos);
      this.vel = port1.transformVel(this.vel);
      return true;
    }
    if(int2){
      this.pos = port2.transformPos(this.pos);
      this.vel = port2.transformVel(this.vel);
      return true;
    }
    return false;
  }
  checkCollisions(ts){
    var projectionRay = ray.fromPoints(this.pos, this.pos.plus(this.vel.multiply(ts)));
    var cols = [];
    for(var i = 0; i < geometries.length; i++){
      var raycol = geometries[i].col.rayIntersect(projectionRay);
      if(raycol){
        if(compareNum(raycol.x, geometries[i].col.left()))
          this.hitRWall(raycol);
        else if(compareNum(raycol.x, geometries[i].col.right()))
          this.hitLWall(raycol);
        else if(compareNum(raycol.y, geometries[i].col.bottom()))
          this.hitCeiling(raycol);
        else
          this.hitGround(raycol);
      }
    }
  }
  checkBorderCollisions(ts){
    var inwall = false;
    if(this.pos.x <= worldLeft){
      this.hitLWall(new vec2(worldLeft, this.pos.y));
      inwall = true;
    }
    if(this.pos.x >= worldRight){
      this.hitRWall(new vec2(worldRight, this.pos.y));
      inwall = true;
    }
    if(this.pos.y <= worldTop){
      this.hitCeiling(new vec2(this.pos.x, worldTop));
      inwall = true;
    }
    if(this.pos.y >= worldBottom){
      this.hitGround(new vec2(this.pos.x, worldBottom));
      inwall = true;
    }
    if(inwall) return;

    var projectionRay = ray.fromPoints(this.pos, this.pos.plus(this.vel.multiply(ts)));
    var borderBounds = box.fromBounds(worldLeft+1, worldRight-1, worldTop+1, worldBottom-1);
    var raycol = borderBounds.rayIntersect(projectionRay);
    if(raycol){
      if(compareNum(raycol.x, borderBounds.left()))
        this.hitLWall(raycol);
      else if(compareNum(raycol.x, borderBounds.right()))
        this.hitRWall(raycol);
      else if(compareNum(raycol.y, borderBounds.bottom()))
        this.hitGround(raycol);
      else
        this.hitCeiling(raycol);
    }
  }
  checkEnemyCollisions(ts){
    var projectionRay = ray.fromPoints(this.pos.minus(this.vel.multiply(ts)), this.pos.plus(this.vel.multiply(ts)));
    for (var i = 0; i < enemies.length; i++){
      if(this._stuckto)
        if(this._stuckto.pos.equals(enemies[i].pos))
          continue;
      var raycols = projectionRay.polygonCollision(enemies[i].body);
      if(raycols.length <= 0) continue;
      for(var i2 = 0; i2 < raycols.length; i2++)
        raycols[i2].hpos = this.pos;
      raycols.sort(function(a,b){
        return a.intersection.distance(a.hpos) - b.intersection.distance(b.hpos);
      });
      this.pos = raycols[0].intersection;
      this.hitEnemy(enemies[i]);
      break;
    }
  }
  hitGround(col){
    this.pos = col;
    var hfact = Math.abs(this.vel.x / this.vel.y);
    if(hfact >= this.ricochetCutoff){
      this.vel.y = -Math.abs(this.vel.y) * this.bounceCoeff;
      this.vel.x *= this.bounceFriction;
      this.pointStats.skipshot += 1;
    }
    else
      this.stick();
  }
  hitCeiling(col){
    this.pos = col;
    var hfact = Math.abs(this.vel.x / this.vel.y);
    if(hfact >= this.ricochetCutoff){
      this.vel.y = Math.abs(this.vel.y) * this.bounceCoeff;
      this.vel.x *= this.bounceFriction;
      this.pointStats.bankshot += 1;
    }
    else{
      this.stick(30);
      this.pointStats.stalactite = 1;
    }
  }
  hitRWall(col){
    this.pos = col;
    var vfact = Math.abs(this.vel.y / this.vel.x);
    if(vfact >= this.ricochetCutoff){
      this.vel.x = -Math.abs(this.vel.x) * this.bounceCoeff;
      this.vel.y *= this.bounceFriction;
      this.pointStats.bankshot += 1;
    }
    else{
      this.stick(120);
      this.pointStats.dropshot = 1;
    }
  }
  hitLWall(col){
    this.pos = col;
    var vfact = Math.abs(this.vel.y / this.vel.x);
    if(vfact >= this.ricochetCutoff){
      this.vel.x = Math.abs(this.vel.x) * this.bounceCoeff;
      this.vel.y *= this.bounceFriction;
      this.pointStats.bankshot += 1;
    }
    else{
      this.stick(120);
      this.pointStats.dropshot = 1;
    }
  }
  hitEnemy(en){
    var dmg = this.vel.distance() / 4 + 2;
    var pointstats = this.pointStats;
    var overkill = en.isdead();
    var vel = this.vel.copy();
    en.arrowHit(this);
    en.damage(dmg, this);
    
    var combo = 0;
    if(pointstats.stalactite){
      this.p_stalactite();
      combo += 1;
    }
    if(pointstats.dropshot){
      this.p_dropshot();
      combo += 1;
    }
    if(pointstats.lobshot){
      this.p_lobshot();
      combo += 1;
    }
    if(pointstats.skipshot){
      this.p_skipshot(pointstats.skipshot);
      combo += 1;
    }
    if(pointstats.bankshot){
      this.p_bankshot(pointstats.bankshot);
      combo += 1;
    }
    if(pointstats.chain){
      this.p_chain(pointstats.chain);
      combo += 1;
    }
    if(vel.distance() > 25){
      this.p_highvel();
      combo += 1;
    }
    if(pointstats.snipe >= 1){
      this.p_snipe();
      combo += 1;
    }
    if(overkill){
      this.p_overkill();
      combo += 1;
    }
    else if(en.punctures.length >= 2){
      this.p_doublepenetration(en.punctures.length - 1);
      combo += 1;
    }
    if(!overkill && en.isdead())
      this.p_killshot();
    else if(combo <= 0)
      this.p_hit();
    
    if(combo >= 2)
      this.p_combo(combo - 1);

    this.loadPointStats(this.pointStats.chain + 1);
  }
  p_stalactite(){
    var pts = 30;
    messageController.addMessage(new message("STALACTITE", this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_dropshot(){
    var pts = 30;
    messageController.addMessage(new message("DROP SHOT", this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_lobshot(){
    var pts = 50;
    messageController.addMessage(new message("LOB SHOT", this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_skipshot(c){
    var pts = 30 * c;
    var x = "";
    if(c > 1)
      x = " x" + c.toString();
    messageController.addMessage(new message("SKIPSHOT" + x, this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_bankshot(c){
    var pts = 40 * c;
    var x = "";
    if(c > 1)
      x = " x" + c.toString();
    messageController.addMessage(new message("BANKSHOT" + x, this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_chain(c){
    var pts = 60 * c;
    var x = "";
    if(c > 1)
      x = " x" + c.toString();
    messageController.addMessage(new message("CHAIN REACTION" + x, this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_hit(){
    var pts = 10;
    messageController.addMessage(new message("HIT", this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_killshot(){
    var pts = 50;
    messageController.addMessage(new message("KILLSHOT", this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_highvel(){
    var pts = 20;
    messageController.addMessage(new message("POWERSHOT", this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_snipe(){
    var pts = 30;
    messageController.addMessage(new message("SNIPE", this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_overkill(){
    var pts = 50;
    messageController.addMessage(new message("OVERKILL", this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_doublepenetration(c){
    var pts = 50 * c;
    var x = "DOUBLE";
    if(c > 1)
      x = (c + 1).toString() + "x";
    messageController.addMessage(new message(x + " PENETRATION", this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }
  p_combo(c){
    var pts = 50 * c;
    var x = (c + 1).toString() + "x ";
    messageController.addMessage(new message(x + "COMBO", this.pos.copy()).pointFormat());
    messageController.addMessage(new message("+" + pts.toString(), this.pos.copy()).pointFormat());
    addScore(pts);
  }

  update(ts){
    var tsi = ts;
    ts = this.getRelTimescale(ts);
    if(!this.active){
      this.beStuck(ts);
      return;
    }
    this.pointStats.snipe += this.vel.multiply(ts * 0.0025).distance();
    this.applyPhysics(ts);
    this._didCollide = false;
    if(!this.checkPortals(ts)){
      this.checkCollisions(ts);
      this.checkBorderCollisions(ts);
    }
    if(!this._istracer){
      //this.checkCollisions(ts);
      //this.checkBorderCollisions(ts);
      this.checkEnemyCollisions(ts);
    }
    if(!this._didCollide)
      super.update(tsi);
  }
  draw(ctx){
    super.draw(ctx);
    this.body.setPosition(this.pos);
    if(this._stucktime <= 0)
      this.body.setRotation(this.vel.direction());
    this.body.drawFill(ctx, "#ddd");
    this.body.drawOutline(ctx, "#222", 2);
  }
}
class portalProj extends arrow{
  constructor(pos, ang, pow){
    super(pos, ang, pow);
    this.body = null;
    this.num = port1 == null ? 1 : 2;
  }
  hitEnemy(){
    return;
  }
  checkCollisions(ts){
    var projectionRay = ray.fromPoints(this.pos, this.pos.plus(this.vel.multiply(ts)));
    var cols = [];
    for(var i = 0; i < geometries.length; i++){
      var raycol = geometries[i].col.rayIntersect(projectionRay);
      if(raycol)
        this.hit(raycol, geometries[i]);
    }
  }
  checkBorderCollisions(ts){
    var projectionRay = ray.fromPoints(this.pos, this.pos.plus(this.vel.multiply(ts)));
    var borderBounds = box.fromBounds(worldLeft, worldRight, worldTop, worldBottom);
    var raycol = borderBounds.rayIntersect(projectionRay);
    if(raycol){
      if(compareNum(raycol.x, borderBounds.left()))
        this.hit(raycol);
      else if(compareNum(raycol.x, borderBounds.right()))
        this.hit(raycol);
      else if(compareNum(raycol.y, borderBounds.bottom()))
        this.hit(raycol);
      else
        this.hit(raycol);
    }
  }

  hit(colpoint, geom = null){
    this._didCollide = true;
    if(this._istracer)
      return;
    var port = portal.fromcol(geom, colpoint)
    if(port){
      if(!port1){
        port.num = 1;
        port1 = port;
      }
      else if(!port2){
        port.num = 2;
        port2 = port;
      }
    }

    arrows.splice(arrows.indexOf(this), 1)
  }
  applyPhysics(ts){
    var friction = 1;
    this.vel = this.vel.multiply(((friction - 1) * ts) + 1);
    this.vel.y += (gravity * 2) * ts;
  }

  update(ts){
    var tsi = ts;
    ts = this.getRelTimescale(ts);

    //this.applyPhysics(ts);
    //this.checkCollisions(ts);
    //this.checkBorderCollisions(ts);

    super.update(tsi);
  }
  draw(ctx){
    ctx.fillStyle = this.num == 1 ? "#55f" : "#fb5";
    ctx.strokeStyle = this.num == 1 ? "#33b" : "#b63";
    ctx.lineWidth = 2;

    ctx.beginPath();
    ctx.arc(this.pos.x, this.pos.y, 7, 0, Math.PI * 2);
    ctx.fill();
    ctx.stroke();
  }
}
class player extends thing{
  constructor(pos = new vec2()){
    super(pos);
    this.col = new box(0, 0, 20, 40);
    this.col.centerAt(this.pos);
    this.speed = 5;
    this.ammo = 1;
    this.flipped = false;
    this.equipped = 2;
    this._aim = 0;
    this._aimSpd = -0.01;
    this._acc = 1;
    this._onGround = false;
    this._pullBack = 0;
    this._pullBack2 = 0;
    this._maxPullback = 120;
    this._arrpos = new vec2(0, -10);
    this._isTeleporting = 0;
    this._dead = 0;
    this._gibs = [];
  }
  
  getTrueAim(){
    if(this.flipped)
      return Math.PI - this._aim;
    return this._aim;
  }
  getTrueSpeed(){
    if(this._pullBack > 0)
      return this.speed / 3;
    return this.speed;
  }
  control(ts){
    if(controlsPressed.right) this.moveRight(ts);
    if(controlsPressed.left) this.moveLeft(ts)
    
    if(controlsPressed.up) this.moveUp(ts);
    else if(controlsPressed.down) this.moveDown(ts);
    else this._aimSpd = 0.01;

    if(controlsPressed.jump) this.jump(ts);
    if(controlsPressed.shoot) this.shoot(ts);
    else if(this._pullBack > 0) this.fire(ts);
    if(controlsPressed.melee) this.utility(ts);
    else if(this._pullBack2 > 0) this.utilRelease(ts);
  }
  applyPhysics(ts){
    var friction = 0.8;
    this.vel.y += gravity * ts;
    if(this._onGround)
      this.vel.x *= ((friction - 1) * ts) + 1
  }
  checkPortals(ts){
    if(!(port1 && port2))
      return false;

    var int1 = box.intersection(this.col, port1.col);
    var int2 = box.intersection(this.col, port2.col);

    if(!int1.isEmpty()){
      if(port1.isVertical()){
        /*if(compareNum(int1.size.x, port1.col.size.x)){
          if(compareNum(int1.bottom(), port1.col.bottom())){
            if(this.col.bottom() - int1.bottom() <= 10){
              this.hitGround(int1.bottom());
              int1.size.y = this.col.size.y;
            }
          }
          if(compareNum(int1.top(), port1.col.top())){
            if(int1.top() - this.col.top() <= 10){
              this.hitCeiling(int1.top());
              int1.size.y = this.col.size.y;
            }
          }
        }*/
        if(compareNum(int1.size.y, this.col.size.y)){
          this.vel.y = 0;
          this._isTeleporting = 1;
          if(port1.facedir.equals(vec2.left)){
            if(this.pos.x > port1.col.left()){
              this.pos = port1.transformPos(this.pos, 1, true);
              this.vel = port1.transformVel(this.vel);
              this._isTeleporting = 2;
            }
          }
          else{
            if(this.pos.x < port1.col.right()){
              this.pos = port1.transformPos(this.pos, 1, true);
              this.vel = port1.transformVel(this.vel);
              this._isTeleporting = 2;
            }
          }
          return true;
        }
      }
      else{
        if(compareNum(int1.size.y, port1.col.size.y)){
          if(compareNum(int1.left(), port1.col.left())){
            if(int1.left() - this.col.left() <= 6){
              this.hitLWall(int1.left());
              int1.size.x = this.col.size.x;
            }
          }
          else if(compareNum(int1.right(), port1.col.right())){
            if(this.col.right() - int1.right() <= 6){
              this.hitRWall(int1.right());
              int1.size.x = this.col.size.x;
            }
          }
        }
        if(compareNum(int1.size.x, this.col.size.x)){
          this._isTeleporting = 1;
          if(port1.facedir.equals(vec2.up)){
            if(this.pos.y > port1.col.top()){
              this.pos = port1.transformPos(this.pos, 1, true);
              this.vel = port1.transformVel(this.vel);
              this._isTeleporting = 2;
            }
          }
          else{
            if(this.pos.y < port1.col.bottom()){
              this.pos = port1.transformPos(this.pos, 1, true);
              this.vel = port1.transformVel(this.vel);
              this._isTeleporting = 2;
            }
          }
          return true;
        }
      }
    }
    else if(!int2.isEmpty()){
      if(port2.isVertical()){
        /*if(compareNum(int2.size.x, port2.col.size.x)){
          if(compareNum(int2.bottom(), port2.col.bottom())){
            if(this.col.bottom() - int2.bottom() <= 10){
              this.hitGround(int2.bottom());
              int2.size.y = this.col.size.y;
            }
          }
          if(compareNum(int2.top(), port2.col.top())){
            if(int2.top() - this.col.top() <= 10){
              this.hitCeiling(int2.top());
              int2.size.y = this.col.size.y;
            }
          }
        }*/
        if(compareNum(int2.size.y, this.col.size.y)){
          this.vel.y = 0;
          this._isTeleporting = 2;
          if(port2.facedir.equals(vec2.left)){
            if(this.pos.x > port2.col.left()){
              this.pos = port2.transformPos(this.pos, 1, true);
              this.vel = port2.transformVel(this.vel);
              this._isTeleporting = 1;
            }
          }
          else{
            if(this.pos.x < port2.col.right()){
              this.pos = port2.transformPos(this.pos, 1, true);
              this.vel = port2.transformVel(this.vel);
              this._isTeleporting = 1;
            }
          }
          return true;
        }
      }
      else{
        if(compareNum(int2.size.y, port2.col.size.y)){
          if(compareNum(int2.left(), port2.col.left())){
            if(int2.left() - this.col.left() <= 6){
              this.hitLWall(int2.left());
              int2.size.x = this.col.size.x;
            }
          }
          else if(compareNum(int2.right(), port2.col.right())){
            if(this.col.right() - int2.right() <= 6){
              this.hitRWall(int2.right());
              int2.size.x = this.col.size.x;
            }
          }
        }
        if(compareNum(int2.size.x, this.col.size.x)){
          this._isTeleporting = 2;
          if(port2.facedir.equals(vec2.up)){
            if(this.pos.y > port2.col.top()){
              this.pos = port2.transformPos(this.pos, 1, true);
              this.vel = port2.transformVel(this.vel);
              this._isTeleporting = 1;
            }
          }
          else{
            if(this.pos.y < port2.col.bottom()){
              this.pos = port2.transformPos(this.pos, 1, true);
              this.vel = port2.transformVel(this.vel);
              this._isTeleporting = 1;
            }
          }
          return true;
        }
      }
    }

    this._isTeleporting = 0;
    return false;
  }
  checkEnemyCollisions(){
    for(var i in enemies){
      if(this.col.polygonIntersections(enemies[i].body).length > 0)
        this.kill();
    }
  }
  checkCollisions(ts){
    for(var i = 0; i < geometries.length; i++){
      var intside = geometries[i].testCollision(this.col);
      if(!intside)
        continue;
      switch(intside){
        case geometry.top:
          this.hitGround(geometries[i].col.top());
          break;
        case geometry.bottom:
          this.hitCeiling(geometries[i].col.bottom());
          break;
        case geometry.right:
          this.hitLWall(geometries[i].col.right());
          break;
        case geometry.left:
          this.hitRWall(geometries[i].col.left());
          break;
      }
    }
  }
  checkBorderCollisions(ts){
    if(this.col.bottom() >= worldBottom)
      this.hitGround(worldBottom);
    if(this.col.top() <= worldTop)
      this.hitCeiling(worldTop);
    if(this.col.right() >= worldRight)
      this.hitRWall(worldRight);
    if(this.col.left() <= worldLeft)
      this.hitLWall(worldLeft);
  }
  pickUpArrows(){
    for(var i = 0; i < arrows.length; i ++){
      if(arrows[i].active)
        continue;
      if(arrows[i]._stuckto)
        continue;
      this.col.size.y += 5;
      if(this.col.containsPoint(arrows[i].pos)){
        arrows.splice(i, 1)
        this.ammo++;
      }
      this.col.size.y -= 5;
    }
  }

  moveRight(ts){
    this.flipped = false;
    if(this.vel.x < this.getTrueSpeed()){
      this.vel.x += this._acc * ts;
      if(this.vel.x > this.getTrueSpeed())
        this.vel.x = this.getTrueSpeed();
    }
  }
  moveLeft(ts){
    this.flipped = true;
    if(this.vel.x > -this.getTrueSpeed()){
      this.vel.x -= this._acc * ts;
      if(this.vel.x < -this.getTrueSpeed())
        this.vel.x = -this.getTrueSpeed();
    }
  }
  moveUp(ts){
    if(this._pullBack <= 0 && this._pullBack2 <= 0)
      return;
    var acceleration = 0.005;
    this._aim -= this._aimSpd * ts;
    this._aimSpd += acceleration * ts;
    if(this._aim < -Math.PI / 2)
      this._aim = -Math.PI / 2;
  }
  moveDown(ts){
    if(this._pullBack <= 0 && this._pullBack2 <= 0)
      return;
    var acceleration = 0.005;
    this._aim += this._aimSpd * ts;
    this._aimSpd += acceleration * ts;
    if(this._aim > Math.PI / 2)
      this._aim = Math.PI / 2;
  }
  jump(ts){
    var jumpStrength = 10;
    var jumpSustain = 0.6;
    if(this._onGround){
      this.vel.y -= jumpStrength;
      this._onGround = false;
    }
    if(this.vel.y < 0)
      this.vel.y -= (gravity * ts) * jumpSustain;
  }
  shoot(ts){
    if(this.ammo <= 0)
      return;
    if(this._pullBack < this._maxPullback){
      this._pullBack += ts;
      if(this._pullBack >= this._maxPullback)
        this._pullBack = this._maxPullback;
    }
  }
  fire(ts){
    if(this._pullBack < 10){
      this._aim = 0;
      this._aimSpd = 0;
      this._pullBack = 0;
      return;
    }
    var arr = new arrow(this.pos.plus(this._arrpos), this.getTrueAim(), this._pullBack / 4);
    arrows.push(arr);
    this._aim = 0;
    this._aimSpd = 0;
    this._pullBack = 0;
    this.ammo -= 1;
  }
  utility(ts){
    if(port1 && port2){
      this._pullBack2 = 1;
      return;
    }
    if(this._pullBack2 < this._maxPullback){
      this._pullBack2 += ts;
      if(this._pullBack2 >= this._maxPullback)
        this._pullBack2 = this._maxPullback;
    }
  }
  utilRelease(ts){
    if(port1 && port2)
      destroyPortals();
    if(this._pullBack2 < 10){
      this._aim = 0;
      this._aimSpd = 0;
      this._pullBack2 = 0;
      return;
    }
    var prt = new portalProj(this.pos.plus(this._arrpos), this.getTrueAim(), this._pullBack2 / 4);
    arrows.push(prt);
    this._aim = 0;
    this._aimSpd = 0;
    this._pullBack2 = 0;
  }
  kill(){
    this._dead = 240;
    for (var i in this._gibs)
      this._gibs[i].size = 0;
    this._gibs = [];
    for(var i = 30; i > 0; i--){
      var pg = new pgib(this.pos.plus(vec2.fromAng(Math.PI * 2 * Math.random(), Math.random() * 10)), vec2.fromAng(Math.PI * 2 * Math.random(), Math.random() * 10));
      gibs.push(pg);
      this._gibs.push(pg);
    }
    if(lives >= 1)
      messageController.addMessage(new message("-1 LIFE", this.pos, 120).badFormat());
  }
  respawn(){
    this._dead = 0;
    this.vel = new vec2();
    lives -= 1;
  }
  deadUpdate(ts){
    if(lives <= 0)
      return;
    this._dead -= ts;
    if(this._dead < 60)
      this.attractGibs(ts, false);
    if(this._dead <= 0)
      this.respawn();
  }
  attractGibs(ts, kill = true){
    for (var i = this._gibs.length - 1; i >= 0; i--) {
      this._gibs[i].seek(this.pos.plus(new vec2(Math.random() * 20 - 10, Math.random() * 40 - 20)), ts, kill ? 20 : 0);
      if(this._gibs[i].size <= 0)
        this._gibs.splice(i, 1);
    }
  }
  hitGround(y){
    this.pos.y -= this.col.bottom() - y;
    this.vel.y = 0;
    this._onGround = true;
  }
  hitCeiling(y){
    this.pos.y += y - this.col.top() + 1;
    this.vel.y = 0;
  }
  hitLWall(x){
    this.pos.x += x - this.col.left();
    this.vel.x = 0;
  }
  hitRWall(x){
    this.pos.x -= this.col.right() - x;
    this.vel.x = 0;
  }

  drawBow(ctx){
    /*
    if(this._pullBack <= 0)
      return;
    var lengthdiv = 4;

    ctx.strokeStyle = "#141";
    ctx.lineWidth = 6;
    var pn1 = this.pos.plus(this._arrpos).plus(vec2.fromAng(this.getTrueAim(), (this._maxPullback - this._pullBack) / lengthdiv));
    var pn2 = this.pos.plus(this._arrpos).plus(vec2.fromAng(this.getTrueAim(), this._maxPullback / lengthdiv));
    ctx.beginPath();
    ctx.moveTo(pn1.x, pn1.y);
    ctx.lineTo(pn2.x, pn2.y);
    ctx.stroke();

    ctx.strokeStyle = "#4d4";
    ctx.lineWidth = 2;
    pn1 = this.pos.plus(this._arrpos);
    pn2 = pn1.plus(vec2.fromAng(this.getTrueAim(), this._pullBack / lengthdiv));
    ctx.beginPath();
    ctx.moveTo(pn1.x, pn1.y);
    ctx.lineTo(pn2.x, pn2.y);
    ctx.stroke();
    */
    this.drawArrowPath(ctx);
  }
  drawArrowPath(ctx){
    if(this._pullBack < 10)
      return;

    var aimIterations = 20;
    var ga = new arrow(this.pos.plus(this._arrpos), this.getTrueAim(), this._pullBack / 4);
    ga._istracer = true;
    ctx.lineWidth = 4;
    var p = this.pos.plus(this._arrpos);
    for(var i = aimIterations; i > 0; i--){
      var alpha = i / aimIterations;
      ctx.strokeStyle = "rgba(255,255,255," + alpha.toString() +")";
      ctx.beginPath();
      ga.update(1);
      ctx.moveTo(ga.pos.x, ga.pos.y - 2);
      ctx.lineTo(ga.pos.x, ga.pos.y + 2);
      ctx.stroke();
    }
  }
  drawArrowPath2(ctx){
    if(this._pullBack2 < 10)
      return;

    var aimIterations = 20;
    var ga = new portalProj(this.pos.plus(this._arrpos), this.getTrueAim(), this._pullBack2 / 4);
    ga._istracer = true;
    ctx.lineWidth = 4;
    var p = this.pos.plus(this._arrpos);
    for(var i = aimIterations; i > 0; i--){
      var alpha = i / aimIterations;
      ctx.strokeStyle = "rgba(255,255,255," + alpha.toString() +")";
      ctx.beginPath();
      ga.update(1);
      ctx.moveTo(ga.pos.x, ga.pos.y - 2);
      ctx.lineTo(ga.pos.x, ga.pos.y + 2);
      ctx.stroke();
      if(ga._didCollide)
        break;
    }
  }
  drawTeleporting(ctx){
    var b1 = new box(this.col.left(), this.col.top(), this.col.size.x, this.col.size.y);
    var b2 = new box(this.col.left(), this.col.top(), this.col.size.x, this.col.size.y);
    var port = portal.byNum(this._isTeleporting);
    var oport = port.otherPortal();
    b2.centerAt(oport.col.center());

    if(port.isVertical()){
      if(port.facedir.equals(vec2.left)){
        b1.size.x = port.col.right() - this.col.left();
      }
      else{
        b1.position.x = port.col.left();
        b1.size.x = this.col.right() - port.col.left();
      }
    }
    else{
      if(port.facedir.equals(vec2.up)){
        b1.size.y = port.col.bottom() - this.col.top();
      }
      else{
        b1.position.y = port.col.top();
        b1.size.y = this.col.bottom() - port.col.top();
      }
    }

    /*
    if(oport.isVertical()){
      if(oport.facedir.equals(vec2.left)){
        b2.size.x = this.col.right() - port.col.left();
        b2.position.x = oport.col.right(); 
      }
      else{
        b2.position.x = oport.col.right();
        b2.size.x = this.col.right() - port.col.left();
      }
    }
    else{
      if(oport.facedir.equals(vec2.up)){
        b2.size.y = port.col.bottom() - this.col.top();
      }
      else{
        b2.position.y = oport.col.top();
        b2.size.y = port.col.top() - this.col.bottom();
      }
    }
    */

    b1.drawFill(ctx, "#66d");
    b1.drawOutline(ctx, "#113", 2);
    //b2.drawFill(ctx, "#66d");
    //b2.drawOutline(ctx, "#113", 2);
    this.drawBow(ctx);
    this.drawArrowPath2(ctx);
  }

  update(ts){
    if(this._dead){
      this.deadUpdate(ts);
      return;
    }
    var tsi = ts;
    ts = this.getRelTimescale(ts);
    this.attractGibs(ts);
    this.applyPhysics(ts);
    this._onGround = false;
    this.pickUpArrows();
    if(!this.checkPortals(ts)){
      this.checkCollisions(ts);
      this.checkBorderCollisions(ts);
    }
    this.checkEnemyCollisions();
    this.control(ts);
    super.update(tsi);
    this.col.centerAt(this.pos);
  }
  draw(ctx){
    if(this._dead)
      return;
    super.draw(ctx);
    if(this._isTeleporting){
      this.drawTeleporting(ctx)
      return;
    }
    this.col.drawFill(ctx, "#66d");
    this.col.drawOutline(ctx, "#113", 2);
    this.drawBow(ctx);
    this.drawArrowPath2(ctx);
  }
}
class gib extends thing{
  constructor(pos, vel = new vec2(), size  = 5){
    super(pos);
    this.vel = vel;
    this.rotVel = Math.random() * 0.8 - 0.4;
    this.size = size;
    this._osize = size;
    this.loadBody(size);
  }
  loadBody(size = 5){
    this.body = new polygon();
    var verts = [];
    var segs = Math.floor(Math.random() * 3 + 3);
    for(var i = segs; i > 0; i--){
      var ang = i / segs * Math.PI * 2;
      verts.push(vec2.fromAng(ang, size));
    }
    this.body.setVerts(verts);
  }
  
  applyFriction(ts){
    var linearFriction = 0.99;
    var rotationFriction = 0.98;
    this.vel = this.vel.multiply((linearFriction - 1) * ts + 1);
    this.rotVel *= (rotationFriction - 1) * ts + 1;
  }
  shrink(ts, index){
    var shrinkSpeed = 0.95;
    var factor = (shrinkSpeed - 1) * ts + 1;
    this.size *= factor;
    this.body.setScale(this.body.getScale() * factor);
    if(this.size < 1)
      gibs.splice(index, 1);
  }
  checkPortals(){
    if(!(port1 && port2))
      return false;
    var int1 = box.intersection(this.col, port1.col);
    var int2 = box.intersection(this.col, port2.col);
    if(!int1.isEmpty()){
      if(port1.isVertical()){
        if(compareNum(int1.size.y, this.col.size.y)){
          if(Math.sign(this.vel.x) != port1.facedir.x){
            this.pos = port1.transformPos(this.pos);
            this.vel = port1.transformVel(this.vel);
            return true;
          }
        }
      }
      else{
        if(compareNum(int1.size.x, this.col.size.x)){
          if(Math.sign(this.vel.y) != port1.facedir.y){
            this.pos = port1.transformPos(this.pos);
            this.vel = port1.transformVel(this.vel);
            return true;
          }
        }
      }
    }
    else if(!int2.isEmpty()){
      if(port2.isVertical()){
        if(compareNum(int2.size.y, this.col.size.y)){
          if(Math.sign(this.vel.x) != port2.facedir.x){
            this.pos = port2.transformPos(this.pos);
            this.vel = port2.transformVel(this.vel);
            return true;
          }
        }
      }
      else{
        if(compareNum(int2.size.x, this.col.size.x)){
          if(Math.sign(this.vel.y) != port2.facedir.y){
            this.pos = port2.transformPos(this.pos);
            this.vel = port2.transformVel(this.vel);
            return true;
          }
        }
      }
    }

    return false;
  }
  checkCollisions(index){
    var sizefact = this.size * 1.5;
    this.col = new box(this.pos.x - sizefact / 2, this.pos.y - sizefact / 2, sizefact, sizefact);
    if(!this.checkPortals()){
      this.checkGeomCollisions();
      this.checkBorderCollisions(index);
    }
  }
  checkGeomCollisions(){
    for(var i = 0; i < geometries.length; i++){
      var intside = geometries[i].testCollision(this.col);
      if(!intside)
        continue;
      switch(intside){
        case geometry.top:
          this.hitGround(geometries[i].col.top());
          break;
        case geometry.bottom:
          this.hitCeiling(geometries[i].col.bottom());
          break;
        case geometry.right:
          this.hitLWall(geometries[i].col.right());
          break;
        case geometry.left:
          this.hitRWall(geometries[i].col.left());
          break;
      }
    }
  }
  checkBorderCollisions(index){
    if(this.col.bottom() > worldBottom){
      this.hitGround(worldBottom);
    }
    if(this.pos.x > worldRight) gibs.splice(index, 1)
    if(this.pos.x < worldLeft) gibs.splice(index, 1)
  }
  hitGround(y){
    this.pos.y -= this.col.bottom() - y;
    this.vel.y = -Math.abs(this.vel.y) * 0.55;
    this.vel.x *= 0.8;
  }
  hitCeiling(y){
    this.pos.y += y - this.col.top() + 1;
    this.vel.y = Math.abs(this.vel.y) * 0.55;
    this.vel.x *= 0.8;
  }
  hitLWall(x){
    this.pos.x += x - this.col.left();
    this.vel.x = Math.abs(this.vel.x) * 0.7;
  }
  hitRWall(x){
    this.pos.x -= this.col.right() - x;
    this.vel.x = -Math.abs(this.vel.x) * 0.7;
  }

  update(ts, index){
    var tsi = ts;
    ts = this.getRelTimescale(ts);
    this.checkCollisions(index);
    if(this.vel.distance() < 0.2)
      this.shrink(ts, index);
    this.applyFriction(ts);
    this.vel.y += gravity * ts;
    this.body.setRotation(this.body.getRotation() + this.rotVel * ts);
    super.update(tsi);
  }

  drawFill(ctx){
    this.body.drawFill(ctx, "#f99");
  }
  drawOutline(ctx){
    this.body.setPosition(this.pos);
    this.body.drawOutline(ctx, "#311", 4);
  }
}
class pgib extends gib{
  constructor(pos, vel = new vec2(), size = 10){
    super(pos, vel, size);
    this.loadbody();
    this.seeking = false;
  }
  loadbody(size = 10){
    this.body = new polygon();
    var verts = [];
    var segs = 4;
    for(var i = segs; i > 0; i--){
      var ang = i / segs * Math.PI * 2;
      verts.push(vec2.fromAng(ang + Math.PI / 4, size));
    }
    this.body.setVerts(verts);
  }
  shrink(ts, index){
    if(this.size < 1)
      gibs.splice(index, 1);
  }
  drawFill(ctx){
    this.body.drawFill(ctx, "#66d");
  }
  drawOutline(ctx){
    this.body.setPosition(this.pos);
    this.body.drawOutline(ctx, "#113", 4);
  }  
  checkBorderCollisions(index){
    if(this.col.bottom() > worldBottom){
      this.hitGround(worldBottom);
    }
  }
  hitGround(y){
    this.rotVel = 0;
    this.body.setRotation(0);
    this.pos.y -= this.col.bottom() - y;
    this.vel.y = -Math.abs(this.vel.y) * 0.55;
    this.vel.x *= 0.8;
  }
  seek(pos, ts, cdist = 0){
    this.seeking = true;
    this.vel = this.vel.multiply((0.8 - 1) * ts + 1);
    this.vel = this.vel.plus(pos.minus(this.pos).normalized(ts * 1.5));
    if(this.pos.distance(pos) < cdist)
      this.size = 0;
  }
  update(ts, index){
    ts = this.getRelTimescale(ts);
    if(!this.seeking)
      this.checkCollisions(index);
    this.shrink(ts, index);
    this.applyFriction(ts);
    if(!this.seeking)
      this.vel.y += gravity * ts;
    this.body.setRotation(this.body.getRotation() + this.rotVel * ts);
    this.pos = this.pos.plus(this.vel.multiply(ts));
  }
}
class enemy extends thing{
  constructor(pos = new vec2(), size = 20){
    super(pos);
    this.size = size;
    this._osize = size;
    this.loadBody();
    this.body.setRotation(Math.random() * Math.PI * 2);
    this.rotVel = 0;
    this.health = size / 2 - 8;
    this.speedLimit = 2 + this.size / 10;
    this.punctures = [];
    this.isBoss = false;
  }

  loadBody(){
    this.body = new polygon();
    this.overts = [];
    var segs = Math.floor(Math.random() * 4 + 3);
    for(var i = segs; i >= 0; i--){
      var ang = i / segs * Math.PI * 2;
      this.overts.push(vec2.fromAng(ang, 1));
    }
    this.body.setVerts(this.overts);
    this.body.transform(new vec2(), 0, this.size);
  }
  
  setSize(newSize){
    this.size = newSize;
    this._osize = newSize;
    this.body.setVerts(this.overts);
    this.body.transform(new vec2(), 0, this.size);
    this.speedLimit = 2 + this.size / 10;
  }
  setScale(scale){
    this.size = this._osize * scale;
    this.body.setScale(scale);
  }
  damage(dmg, arrow = null){
    this.health -= dmg;
    if(!arrow)
      return;
    if(this.isdead()){
      for (var i = this.punctures.length - 1; i >= 0; i--) {
        this.punctures[i]._stucktime = Infinity;
      }
    }
  }
  arrowHit(arrow){
    this.pointImpulse(arrow.pos, arrow.vel.multiply(2));
    var gs = arrow.vel.distance() / 4 + 2;
    for(var i = gs; i >= 0; i--){
      var f = arrow.vel.distance();
      var g = new gib(arrow.pos, vec2.fromAng(arrow.pos.direction(this.pos), f * 0.4), Math.random() * 2.5 + 6.5);
      g.vel = g.vel.plus(vec2.fromAng(Math.random() * Math.PI * 2, Math.random() * (f * 0.2)));
      gibs.push(g);
    }
    arrow.stick(arrow.vel.distance() * 2 + 20, this);
    this.punctures.push(arrow);
  }
  enforceSpeedLimit(ts){
    var damping = 0.9;
    if(this.vel.distance() > this.speedLimit)
      this.vel = this.vel.multiply((damping - 1) * ts + 1);
  }
  pointImpulse(worldpoint, forcevect){
    this.vel = this.vel.plus(forcevect.multiply(0.3));
    var forcedir = forcevect.direction();
    var rv = .2;
    var posProjectionRay = new ray(this.pos, forcedir + (Math.PI / 2), 200000);

    var projectedRay = new ray(worldpoint, forcedir, 200000);
    projectedRay.setPosition(projectedRay.getPosition().minus(vec2.fromAng(projectedRay.getAngle(), 100000)));

    var projectionPos = projectedRay.intersection(posProjectionRay);
    var dir = -1;
    if(!projectionPos){
      var ang = forcedir - Math.PI / 2;
      var negProjectionRay = new ray(this.pos, ang, 200000);
      negProjectionRay.setAngle(ang);
      projectionPos = projectedRay.intersection(negProjectionRay);
      dir = 1;
    }
    if(!projectionPos)
      return;
    var dist = projectionPos.distance(this.pos);
    var emph = 1 - Math.min(1 / (dist / (this.size / 4)), 1);
    rv *= emph * dir;
    rv *= forcevect.distance() / 15;
    
    this.rotVel += rv;
  }
  applyFriction(ts){
    var linearFriction = 1;
    var rotationFriction = 0.98;
    if(this.isdead()){
      linearFriction = 0.97;
      if(this.vel.distance > 10)
        linearFriction = 0.9;
      rotationFriction = 0.9;
    }
    this.vel = this.vel.multiply(((linearFriction - 1) * ts) + 1);
    this.rotVel *= ((rotationFriction - 1) * ts) + 1;
  }
  pop(){
    var giblets = 20;
    for (var i = giblets; i > 0; i--) {
      var g = new gib(this.pos, this.vel);
      g.vel = g.vel.plus(vec2.fromAng(Math.random() * Math.PI * 2, 10));
      gibs.push(g);
    }

    for(var i = this.punctures.length - 1; i >= 0; i--){
      this.punctures[i].vel = this.punctures[i].vel.plus(this.vel);
      this.punctures[i].unstick(6);
    }
    enemies.splice(enemies.indexOf(this), 1);
    if(this.isBoss)
      this.bossKill();
  }
  bossKill(){
    spawnController.main.bossAlive = false;
  }
  deadUpdate(ts){
    var shrinkSpd = 0.99;
    for(var i = 0; i < this.punctures.length; i++){
      this.pointImpulse(this.punctures[i].pos, vec2.fromAng(this.punctures[i].body.getRotation(), ts));
      this.setScale(this.body.getScale() * ((shrinkSpd - 1) * ts + 1));
      if(ts > Math.random() * 2){
        var burstSpeed = 8;
        var burstSpread = 2;
        var g = new gib(
          this.punctures[i].pos, 
          vec2.fromAng(this.punctures[i].body.getRotation() - Math.PI, burstSpeed).plus(this.vel), 
          Math.random() * 3 + 5.5);
        g.vel = g.vel.plus(vec2.fromAng(Math.random() * Math.PI * 2, Math.random() * burstSpread));
        gibs.push(g);
      }
    }
    if(this.size <= 10)
      this.pop();
  }
  isdead(){
    return this.health <= 0;
  }

  doAI(ts){
    this.move(ts);
  }
  move(ts){
    var mag = .3;
    var m = vec2.fromAng(Math.random() * Math.PI * 2, Math.random() * mag);
    this.vel = this.vel.plus(m.multiply(ts));
    this.rotVel += mag * 0.1 * Math.random() * (Math.random() - 0.5) * ts;
  }
  hitPlayer(){
    p1.kill();
  }
  checkBorderCollisions(){
    var pc = false;
    var spd = 1;
    if(this.pos.x < worldLeft){
      this.vel.y = 0;
      this.vel.x = spd;
      pc = true;
    }
    else if(this.pos.x > worldRight){
      this.vel.y = 0;
      this.vel.x = -spd;
      pc = true;
    }
    if(this.pos.y < worldTop){
      this.vel.y = spd;
      this.vel.x = 0;
      pc = true;
    }
    else if(this.pos.y > worldBottom){
      this.vel.y = -spd;
      this.vel.x = 0;
      pc = true;
    }
    if(pc) return;
    var av = this.body.getAbsVerts();
    for(var i = 0; i < av.length; i++){
      if(av[i].x <= worldLeft){
        //this.pos.x += Math.abs(worldLeft - av[i].x);
        this.vel.x = Math.abs(this.vel.x);
	  //console.log("leftHit");
      }
      else if(av[i].x >= worldRight){
        //this.pos.x -= Math.abs(worldRight - av[i].x);
        this.vel.x = -Math.abs(this.vel.x)
	  //console.log("rightHit");
      }
      else if(av[i].y >= worldBottom){
        //this.pos.y -= Math.abs(worldBottom - av[i].y);
        this.vel.y = -Math.abs(this.vel.y);
	  //console.log("bottomHit");
      }
      else if(av[i].y <= worldTop){
        //this.pos.y += Math.abs(worldTop - av[i].y);
        this.vel.y = Math.abs(this.vel.y);
	  //console.log("topHit");
      }
    }
  }
  checkGeometryCollisions(){
    var norm = new vec2();
    for(var i = 0; i < geometries.length; i++){
      var cols = geometries[i].col.polygonIntersections(this.body);
      if(cols.length <= 0)
        continue;
      for(var i2 = 0; i2 < cols.length; i2++){
	  norm = norm.plus(cols[i2].hitside);
      }
    }
    if(!compareNum(norm.x))
      this.vel.x = Math.abs(this.vel.x) * Math.sign(norm.x);
    if(!compareNum(norm.y))
      this.vel.y = Math.abs(this.vel.y) * Math.sign(norm.y);
  }

  static rando(difficulty = 1){
    if(difficulty > 20) difficulty = 20;
    var size = 20;
    if(Math.random() * difficulty >= 3)
      size += 10;
    if(Math.random() * difficulty >= 12)
      size += 10
    var en = new enemy(new vec2(), size);
    return en; 
  }
  static randoBoss(difficulty = 1){
    var size = 30;
    if(Math.random() * difficulty >= 3)
      size += 10;
    if(Math.random() * difficulty >= 5)
      size += 10

    var en = new enemy(new vec2(), size);
    en.isBoss = true;
    return en;
  }

  update(ts){
    var tsi = ts;
    ts = this.getRelTimescale(ts);
    if(this.isdead())
      this.deadUpdate(ts);
    this.enforceSpeedLimit(ts);
    this.checkBorderCollisions();
    this.checkGeometryCollisions();
    this.body.setRotation(this.body.getRotation() + this.rotVel * ts);
    super.update(tsi);
    this.applyFriction(ts);
    this.doAI(ts);
    this.body.setPosition(this.pos);
  }
  drawFill(ctx){
    this.body.drawFill(ctx, "#f99", true);
  }
  drawOutline(ctx){
    this.body.drawOutline(ctx, "#311", 4, true);
  }
}
class spawnController{
  constructor(){
    this.waveNum = 1;
    this.spawnFrequency = 400;
    this.spawnBurstFreq = 190;
    this.spawnBurstLen = 2;
    this.spawnBurstleft = this.spawnBurstLen;
    this.spawnDelay = this.spawnFrequency;
    this.maxSpawn = 1;
    this.waveLength = 3;
    this.waveLeft = this.waveLength;
    this.difficulty = 1;
    this.bossDif = 1;
    this.bossAlive = false;
    this.bossSpawned = false;
    this._tilWave = 0;
  }
  
  static findSpawnPos(){
    var v = new vec2();
    var off = 50;

    var hv = Math.random() < 0.5;
    var ss = Math.random() < 0.5;
    var f = Math.random() * (hv ? worldRight - worldLeft : worldBottom - worldTop);
    if(hv){
      v.x = f;
      v.y = ss ? worldTop - off : worldBottom + off;
    }
    else{
      v.y = f;
      v.x = ss ? worldLeft - off : worldRight + off;
    } 

    return v;
  }
  static validSpawn(en){
    var pbox = en.body.getBoundingBox();
    var pbox = new box(pbox.position.x, pbox.position.y, pbox.size.x, pbox.size.y);
      pbox.size.x = Math.max(pbox.size.x, 1);
      pbox.size.y = Math.max(pbox.size.y, 1);
      console.log(pbox);
    var pcent = pbox.center();
    pbox.centerAt( new vec2(
      Math.min(Math.max(pcent.x, worldLeft), worldRight),
      Math.min(Math.max(pcent.y, worldTop), worldBottom)));
    for(var i in geometries){
      if(box.testOverlap(pbox, geometries[i].col)){
        console.log(false);
        return false;
      }
    }
    return true;
  }
  static spawn(){
    spawnController.main.waveLeft -= 1;
    spawnController.main.spawnBurstLeft -= 1;

    var en = enemy.rando(spawnController.main.difficulty);
    if(!spawnController.main.bossSpawned)
      if(spawnController.main.waveLeft < spawnController.main.waveLength / 3){
        spawnController.spawnBoss();
        en = enemy.randoBoss(spawnController.main.difficulty);
      }

    if(spawnController.main.spawnBurstLeft <= 0){
      spawnController.main.spawnDelay = spawnController.main.spawnFrequency;
      if(enemies.length + spawnController.main.spawnBurstLen <= spawnController.main.maxSpawn)
        spawnController.main.spawnBurstLeft = this.spawnBurstLen * Math.random();
    }
    else
      spawnController.main.spawnDelay = spawnController.main.spawnBurstFreq;
    en.pos = spawnController.findSpawnPos();
    en.body.updateAbsVerts();
    while(!spawnController.validSpawn(en)){
      en.pos = spawnController.findSpawnPos();
      en.body.setPosition(en.pos);
      en.body.updateAbsVerts();
      en.body.updateBoundingBox();
    }
    enemies.push(en);
  }
  static spawnBoss(){
    spawnController.main.bossAlive = true;
    spawnController.main.bossSpawned = true;
  }
  static progress(){
    var middle = new vec2((worldRight - worldLeft) / 2, 200);
    var pts = (spawnController.main.waveNum * 100);
    messageController.addMessage(new message("WAVE " + spawnController.main.waveNum.toString() + " COMPLETE!", middle.copy(), 120).waveFormat());
    messageController.addMessage(new message("+" + pts.toString(), middle.copy(), 120).pointFormat());
    addScore(pts);
    
    levelBuilder.reset();
    spawnController.main._tilWave = 180;
    
    spawnController.main.bossSpawned = false;
    spawnController.main.waveNum += 1.5;
    spawnController.main.difficulty += 1;
    spawnController.main.bossDif += 1;
    spawnController.main.waveLength += 3;
    spawnController.main.waveLeft = spawnController.main.waveLength;
    spawnController.main.maxSpawn += 1;
    spawnController.main.spawnFrequency = ((spawnController.main.spawnFrequency - 60) * 0.9 + 60);
    spawnController.main.spawnBurstFreq = (spawnController.main.spawnBurstFreq - 20) * 0.9 + 20;
    spawnController.main.spawnDelay = spawnController.main.spawnFrequency + 360;
    if(spawnController.main.waveNum % 2 == 0)
      spawnController.main.spawnBurstLen += 1;
    spawnController.main.spawnBurstLeft = spawnController.main.spawnBurstLen;
    
    var m = new message("WAVE " + spawnController.main.waveNum.toString(), middle.copy(), 120).waveFormat();
    m.delay = 300;
    m.add();
  }
  static newWave(){
    if(spawnController.main.waveNum % 5 == 0)
      levelBuilder.transition();
    else
      levelBuilder.transmogrify();
  }

  static init(){
    spawnController.main = new spawnController();
  }
  static update(ts){
    if(spawnController.main._tilWave > 0){
      spawnController.main._tilWave -= ts;
      if(spawnController.main._tilWave <= 0)
        spawnController.newWave();
      return;
    }

    if(enemies.length < spawnController.main.maxSpawn)
      spawnController.main.spawnDelay -= ts;
    if(spawnController.main.spawnDelay <= 0 && spawnController.main.waveLeft > 0){
      if(!spawnController.main.bossAlive)
        spawnController.spawn();
      else if(spawnController.main.spawnBurstLeft > 0)
        spawnController.spawn();
    }

    if(spawnController.main.waveLeft <= 0 && enemies.length <= 0)
      spawnController.progress();
  }
}
class levelBuilder{
  constructor(){
    this.constructs = [];
    this.levels = [];
  }
  
  static init(){
    levelBuilder.main = new levelBuilder();
    levelBuilder.main.loadLevels();
    levelBuilder.transition();
  }
  loadLevels(){
    this.levels.push([
      {required: true, active:true, geom:new geometry(worldMid.x - 50, worldMid.y - 50, 100, 100)},
      {required: true, active:false, geom:new geometry(worldMid.x - 70, worldMid.y + 30, 20, 20)},
      {required: true, active:false, geom:new geometry(worldMid.x + 50, worldMid.y + 30, 20, 20)},
      {required: false, active:false, geom:new geometry(worldMid.x + 50, worldMid.y, 100, 30)},
      {required: false, active:false, geom:new geometry(worldMid.x + 150, worldMid.y, 100, 30)},
      {required: false, active:false, geom:new geometry(worldMid.x - 150, worldMid.y, 100, 30)},
      {required: false, active:false, geom:new geometry(worldMid.x - 250, worldMid.y, 100, 30)},
      {required: false, active:false, geom:new geometry(worldMid.x - 10, worldMid.y + 50, 20, 100)},
      {required: false, active:false, geom:new geometry(worldMid.x - 10, worldMid.y + 150, 20, 100)},
      {required: false, active:false, geom:new geometry(worldMid.x - 100, worldMid.y + 150, 200, 20)},
      {required: false, active:false, geom:new geometry(worldMid.x - 10, worldMid.y - 150, 20, 100)},
      {required: false, active:false, geom:new geometry(worldMid.x - 50, worldMid.y - 150, 100, 20)},
      {required: false, active:false, geom:new geometry(worldLeft, worldTop, 150, 100)},
      {required: false, active:false, geom:new geometry(worldLeft, worldBottom - 100, 150, 100)},
      {required: false, active:false, geom:new geometry(worldRight - 150, worldBottom - 100, 150, 100)},
      {required: false, active:false, geom:new geometry(worldRight - 150, worldTop, 150, 100)},
    ]);
    this.levels.push([
      {required: true, active:true, geom:new geometry(worldMid.x - 150, worldMid.y - 100, 100, 100)},
      {required: true, active:true, geom:new geometry(worldMid.x + 50, worldMid.y - 100, 100, 100)},
      {required: true, active:true, geom:new geometry(worldMid.x - 200, worldMid.y + 50, 175, 100)},
      {required: true, active:true, geom:new geometry(worldMid.x + 25, worldMid.y + 50, 175, 100)},
      {required: false, active:true, geom:new geometry(worldMid.x - 50, worldMid.y - 50, 100, 200)},
      {required: false, active:true, geom:new geometry(worldMid.x - 50, worldMid.y - 100, 100, 50)},
      {required: false, active:true, geom:new geometry(worldMid.x - 150, worldMid.y, 100, 50)},
      {required: false, active:true, geom:new geometry(worldMid.x + 50, worldMid.y, 100, 50)},
      {required: false, active:true, geom:new geometry(worldLeft, worldTop + 150, 100, 20)},
      {required: false, active:true, geom:new geometry(worldRight - 100, worldTop + 150, 100, 20)}
    ]);
    this.levels.push([
      {required: true, active:true, geom:new geometry(worldLeft + 150, worldBottom - 200, 340, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 50, worldBottom - 200, 50, 50)},
      {required: false, active:true, geom:new geometry(worldLeft + 100, worldBottom - 200, 50, 50)},
      {required: false, active:true, geom:new geometry(worldLeft + 50, worldBottom - 150, 50, 50)},
      {required: false, active:true, geom:new geometry(worldLeft + 100, worldBottom - 150, 50, 50)},
      {required: false, active:true, geom:new geometry(worldRight - 150, worldBottom - 200, 50, 50)},
      {required: false, active:true, geom:new geometry(worldRight - 100, worldBottom - 200, 50, 50)},
      {required: false, active:true, geom:new geometry(worldRight - 150, worldBottom - 150, 50, 50)},
      {required: false, active:true, geom:new geometry(worldRight - 100, worldBottom - 150, 50, 50)},
      {required: false, active:true, geom:new geometry(worldLeft, worldTop + 150, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 100, worldTop + 150, 100, 20)},
      {required: false, active:true, geom:new geometry(worldRight - 100, worldTop + 150, 100, 20)},
      {required: false, active:true, geom:new geometry(worldRight - 200, worldTop + 150, 100, 20)},
      {required: false, active:true, geom:new geometry(worldMid.x - 50, worldBottom -250, 50, 50)},
      {required: false, active:true, geom:new geometry(worldMid.x, worldBottom -250, 50, 50)},
      {required: false, active:true, geom:new geometry(worldMid.x - 50, worldBottom -300, 50, 50)},
      {required: false, active:true, geom:new geometry(worldMid.x, worldBottom -300, 50, 50)},
      {required: false, active:true, geom:new geometry(worldMid.x - 75, worldBottom -100, 150, 100)}
    ]);
    this.levels.push([
      {required: false, active:true, geom:new geometry(worldLeft + 70, worldTop + 100, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 170, worldTop + 100, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 270, worldTop + 100, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 370, worldTop + 100, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 470, worldTop + 100, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 70, worldTop + 200, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 170, worldTop + 200, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 270, worldTop + 200, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 370, worldTop + 200, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 470, worldTop + 200, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 70, worldTop + 300, 100, 20)},
      {required: true, active:true, geom:new geometry(worldLeft + 170, worldTop + 300, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 270, worldTop + 300, 100, 20)},
      {required: true, active:true, geom:new geometry(worldLeft + 370, worldTop + 300, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft + 470, worldTop + 300, 100, 20)},
      {required: false, active:true, geom:new geometry(worldLeft, worldTop + 400, 70, 80)},
      {required: false, active:true, geom:new geometry(worldLeft + 70, worldTop + 400, 100, 80)},
      {required: false, active:true, geom:new geometry(worldLeft + 170, worldTop + 400, 100, 80)},
      {required: false, active:true, geom:new geometry(worldLeft + 270, worldTop + 400, 100, 80)},
      {required: false, active:true, geom:new geometry(worldLeft + 370, worldTop + 400, 100, 80)},
      {required: false, active:true, geom:new geometry(worldLeft + 470, worldTop + 400, 100, 80)},
      {required: false, active:true, geom:new geometry(worldLeft + 570, worldTop + 400, 70, 80)}
    ]);
    this.levels.push([
      {required: true, active:true, geom:new geometry(worldLeft + 100, worldBottom - 150, 150, 25)},
      {required: true, active:true, geom:new geometry(worldRight - 250, worldBottom - 150, 150, 25)},
      {required: false, active:true, geom:new geometry(worldLeft + 250, worldBottom - 150, 140, 50)},
      {required: false, active:true, geom:new geometry(worldMid.x - 10, worldBottom - 250, 20, 100)},
      {required: false, active:true, geom:new geometry(worldMid.x - 120, worldBottom - 250, 240, 20)},
      {required: false, active:true, geom:new geometry(worldMid.x - 10, worldBottom - 350, 20, 100)},
      {required: false, active:true, geom:new geometry(worldMid.x - 70, worldBottom - 350, 140, 20)}
    ]);
  }
  
  static merge(){
    console.log("geoms: "+ geometries.length);
    var og = geometries.length;
    var firstrun = true;
    while(geometries.length < og || firstrun){
      firstrun = false;
      og = geometries.length;
      for(var i = geometries.length - 1; i >= 0; i--){
        var geom = geometries[i];
        for(var c = geometries.length - 1; c >= 0; c--){
          if(i == c) continue;
          var test = geometries[c];
          if(geom.col.position.x == test.col.position.x && geom.col.size.x == test.col.size.x){
            if(box.testOverlap(geom.col, test.col)){
              var g = new geometry();
              g.col.position = new vec2(geom.col.position.x, Math.min(geom.col.position.y, test.col.position.y));
              g.col.size = new vec2(geom.col.size.x, Math.max(geom.col.bottom(), test.col.bottom()) - Math.min(geom.col.top(), test.col.top()));
              geometries.splice(Math.max(i, c), 1);
              geometries.splice(Math.min(i, c), 1);
              //console.log(g);
              geometries.push(g);
              geom = geometries[i];
              if(!geom) break;
            }
          }
          else if(geom.col.position.y == test.col.position.y && geom.col.size.y == test.col.size.y){
            if(box.testOverlap(geom.col, test.col)){
              var g = new geometry();
              g.col.position = new vec2(Math.min(geom.col.position.x, test.col.position.x), geom.col.position.y);
              g.col.size = new vec2(Math.max(geom.col.right(), test.col.right()) - Math.min(geom.col.left(), test.col.left()), geom.col.size.y);
              geometries.splice(Math.max(i, c), 1);
              geometries.splice(Math.min(i, c), 1);
              //console.log(g);
              geometries.push(g);
              geom = geometries[i];
              if(!geom) break;
            }
          }
        }
      }
    }
    console.log("merged geoms: "+ geometries.length);
  }
  static reset(){
    geometries = [];
    destroyPortals();
    for (var a in arrows){
      if(arrows[a]._stucktime > 0)
        arrows[a].unstick();
    }
  }
  static transmogrify(){
    geometries = [];
    for(var i = 0; i < levelBuilder.main.constructs.length; i++){
      var active = levelBuilder.main.constructs[i].required;
      if(!active)
        active = Math.random() > .5;
      if(active)
        geometries.push(levelBuilder.main.constructs[i].geom);
    }
    levelBuilder.merge();
    levelBuilder.arrowCheck();
  }
  static arrowCheck(){
    var col = true;
    while(col){
      col = false;
      for(var i in geometries){
        for(var k in arrows){
          console.log(geometries[i].col)
          console.log(arrows[k].pos)
          if(geometries[i].col.containsPoint(arrows[k].pos)){
            col = true;
            if(arrows[k].pos.distance(worldMid) < 50)
              arrows[k].pos = new vec2(Math.random() * (worldRight - worldLeft - 2) + worldLeft + 1, Math.random() * (worldBottom - worldTop - 2) + worldTop + 1);
            else
              arrows[k].pos = arrows[k].pos.plus(vec2.fromAng(arrows[k].pos.direction(worldMid), -50));
 	    arrows[k].unstick();
            console.log("bumped arrow");
          }
        }
      }
    }
  }
  static transition(){
    levelBuilder.main.constructs = levelBuilder.main.levels[
      Math.floor(Math.random() * levelBuilder.main.levels.length)
    ];
    levelBuilder.transmogrify();
  }
}
class portal{
  constructor(){
    this.col = new box();
    this.facedir = new vec2();
    this.geom = null;
    this.num = port1 == null ? 1 : 2;
    this.reoriented = false;
  }
  static fromcol(geom, colpoint){
    var p = null;
    if(!geom){
      if(compareNum(colpoint.y, worldBottom)){
        if(colpoint.x < worldLeft + 25)
          colpoint.x = worldLeft + 25;
        if(colpoint.x > worldRight - 25)
          colpoint.x = worldRight - 25;
        p = portal.fromGround(worldBottom, colpoint.copy());
      }
      if(compareNum(colpoint.x, worldLeft)){
        if(colpoint.y < worldTop + 25)
          colpoint.y = worldTop + 25;
        if(colpoint.y > worldBottom - 25)
          colpoint.y = worldBottom - 25;
        p = portal.fromLWall(worldLeft, colpoint.copy());
      }
      if(compareNum(colpoint.x, worldRight)){
        if(colpoint.y < worldTop + 25)
          colpoint.y = worldTop + 25;
        if(colpoint.y > worldBottom - 25)
          colpoint.y = worldBottom - 25;
        p = portal.fromRWall(worldRight, colpoint.copy());
      }
      if(compareNum(colpoint.y, worldTop)){
        if(colpoint.x < worldLeft + 25)
          colpoint.x = worldLeft + 25;
        if(colpoint.x > worldRight - 25)
          colpoint.x = worldRight - 25;
        p = portal.fromCeiling(worldTop, colpoint.copy());
      }
      return p;
    }
    else{
      if(compareNum(colpoint.y, geom.col.top())){
        if(geom.col.size.x < 50)
          return null;
        if(colpoint.x < geom.col.left() + 25)
          colpoint.x = geom.col.left() + 25;
        if(colpoint.x > geom.col.right() - 25)
         colpoint.x = geom.col.right() - 25;
        p = portal.fromGround(geom.col.top(), colpoint.copy());
      }
      if(compareNum(colpoint.x, geom.col.left())){
        if(geom.col.size.y < 50)
          return null;
        if(colpoint.y < geom.col.top() + 25)
          colpoint.y = geom.col.top() + 25;
        if(colpoint.y > geom.col.bottom() - 25)
          colpoint.y = geom.col.bottom() - 25;
        p = portal.fromRWall(geom.col.left(), colpoint.copy());
      }
      if(compareNum(colpoint.x, geom.col.right())){
        if(geom.col.size.y < 50)
          return null;
        if(colpoint.y < geom.col.top() + 25)
          colpoint.y = geom.col.top() + 25;
        if(colpoint.y > geom.col.bottom() - 25)
          colpoint.y = geom.col.bottom() - 25;
        p = portal.fromLWall(geom.col.right(), colpoint.copy());
      }
      if(compareNum(colpoint.y, geom.col.bottom())){
        if(geom.col.size.x < 50)
          return null;
        if(colpoint.x < geom.col.left() + 25)
          colpoint.x = geom.col.left() + 25;
        if(colpoint.x > geom.col.right() - 25)
          colpoint.x = geom.col.right() - 25;
        p = portal.fromCeiling(geom.col.bottom(), colpoint.copy());
      }
    }
    if(!p)
      return null;
    p.geom = geom;
    p.verify();
    return (p.verify() ? p : null);
  }
  static fromGround(y, mpoint){
    var p = new portal();
    p.facedir = vec2.up.copy();
    p.col = new box(mpoint.x - 25, mpoint.y - 2, 50, 4);
    return p;
  }
  static fromCeiling(y, mpoint){
    var p = new portal();
    p.facedir = vec2.down.copy();
    p.col = new box(mpoint.x - 25, mpoint.y - 2, 50, 4);
    return p;
  }
  static fromLWall(x, mpoint){
    var p = new portal();
    p.facedir = vec2.right.copy();
    p.col = new box(mpoint.x - 2, mpoint.y - 25, 4, 50);
    return p;
  }
  static fromRWall(x, mpoint){
    var p = new portal();
    p.facedir = vec2.left.copy();
    p.col = new box(mpoint.x - 2, mpoint.y - 25, 4, 50);
    return p;
  }
  static byNum(num = 1){
    if(num == 1)
      return port1;
    return port2;
  }
  isVertical(){
    return this.facedir.y == 0;
  }
  otherPortal(){
    return this.num == 1 ? port2 : port1;
  }
  verify(){
    if(this.num != 1)
      if(box.testOverlap(this.col, port1.col) && this.isVertical() == this.otherPortal().isVertical())
        return false;
    var overlaps = [];
    for(var i in geometries){
      var int = box.intersection(this.col, geometries[i].col);
      if(!int.isEmpty())
        overlaps.push(int);
    }
    if(this.isVertical()){
      for(var i in overlaps){
	if(overlaps[i].size.y != this.col.size.y){
          if(this.reoriented)
            return false;
          if(overlaps[i].center().y < this.col.center().y){
            this.reoriented = true;
            this.col.position.y += overlaps[i].bottom() - this.col.top();
          }
          else{
            this.reoriented = true;
            this.col.position.y -= this.col.bottom() - overlaps[i].top();
          }
        }
      }
    }
    else{
      for(var i in overlaps){
        if(overlaps[i].size.x != this.col.size.x){
          if(this.reoriented)
            return false;
	  if(overlaps[i].center().x < this.col.center().x){
	    this.reoriented = true;
	    this.col.position.x += overlaps[i].right() - this.col.left();
	  }
	  else{
	    this.reoriented = true;
	    this.col.position.x -= this.col.right() - overlaps[i].left();
	  }
        }
      }
    }
    return true;
  }
  destroy(){
    switch(this.num){
      case 1: port1 = null; break;
      case 2: port2 = null; break;
    }
  }
  transformVel(vel){
    var yv = 1;
    var xv = 1;
    var nvel = vel.copy();
    if(this.isVertical() == this.otherPortal().isVertical()){
      if(this.facedir.x == this.otherPortal().facedir.x && (this.facedir.x != 0 && this.otherPortal().facedir.x != 0))
        xv = -1;
      if(this.facedir.y == this.otherPortal().facedir.y && (this.facedir.y != 0 && this.otherPortal().facedir.y != 0))
        yv = -1;
      return new vec2(vel.x * xv, vel.y * yv);
    }
    else{
      if(this.isVertical()){
        if(this.facedir.equals(vec2.left)){ //facing left
          if(this.otherPortal().facedir.equals(vec2.up)){
            nvel.y = -Math.abs(vel.x);
            nvel.x = vel.y;
            return nvel;
          }
          else{
            nvel.y = Math.abs(vel.x);
            nvel.x = vel.y;
            return nvel;
          }
        }
        else{                               //facing right
          if(this.otherPortal().facedir.equals(vec2.up)){
            nvel.y = -Math.abs(vel.x);
            nvel.x = vel.y;
            return nvel;
          }
          else{
            nvel.y = Math.abs(vel.x);
            nvel.x = vel.y;
            return nvel;
          }
        }
      }
      else{   //this is horizontal
        if(this.facedir.equals(vec2.up)){
          if(this.otherPortal().facedir.equals(vec2.left)){
            nvel.x = -Math.abs(vel.y);
            nvel.y = vel.x;
            return nvel;
          }
          else{
            nvel.x = Math.abs(vel.y);
            nvel.y = -vel.x;
            return nvel;
          }
        }
        else{ //facing down
          if(this.otherPortal().facedir.equals(vec2.left)){
            nvel.x = -Math.abs(vel.y);
            nvel.y = -vel.x;
            return nvel;
          }
          else{
            nvel.x = Math.abs(vel.y);
            nvel.y = -vel.x;
            return nvel;
          }
        }
      }
    }
    return nvel;
  }
  transformPos(pos, off = 1, center = false){
    if(center)
      return this.otherPortal().col.center().plus(this.otherPortal().facedir.multiply(off));

    var offset = pos.minus(this.col.center());
    if(this.isVertical()){
      if(this.otherPortal().isVertical())
        offset.x = 0;
      else{
        offset.x = offset.y;
        offset.y = 0;
      }
    }
    else{
      if(!this.otherPortal().isVertical())
        offset.y = 0;
      else{
        offset.y = offset.x;
        offset.x = 0;
      }
    }
    return this.otherPortal().col.center().plus(this.otherPortal().facedir.multiply(off).plus(offset));
  }

  draw(ctx){
    var color = this.num == 1 ? "#55f" : "#fb5";
    this.col.drawFill(ctx, color);
  }
}

init();