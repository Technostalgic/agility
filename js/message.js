class messageController{
  static init(){
    messageController.messages = [];
  }

  static addMessage(msg){
    msg.add();
  }  

  static update(ts){
    for(var i = messageController.messages.length - 1; i >= 0; i--){
      messageController.messages[i].update(ts, i);
    }
  }
  static draw(ctx){
    for(var i = messageController.messages.length - 1; i >= 0; i--){
      messageController.messages[i].draw(ctx);
    }
  }
}
messageController.init();
class message{
  constructor(text, pos, time = 60, bypass = false){
    this.delay = 0;
    this.size = 20;
    this.pos = pos;
    this.txt = text;
    this._olife = time;
    this.life = this._olife;
    this.color = "#fff";
    this.flashy = null;
    this.bypass = bypass;
  }

  setColors(col, flash = null){
    this.color = col;
    this.flashy = flash;
    return this;
  }
  pointFormat(){
    this.color = "#0f0";
    this.flashy = "#0a0";
    return this;
  }
  waveFormat(){
    this.color = "#f40";
    this.flashy = "#fc0";
    this.size = 40;
    this.bypass = true;
    return this;
  }
  badFormat(){
    this.setColors("#f33", "#b22");
    return this;
  }
  bonusFormat(){
    this.setColors("#33f", "#99f")
    return this;
  }
  add(){
    var check = !this.bypass;
    while(check){
      check = false;
      if(this.pos.x < 50)
        this.pos.x = 50;
      if(this.pos.x > width - 50)
        this.pos.x = width - 50;

      for(var i = messageController.messages.length - 1; i >= 0; i--){
        if(this.pos.equals(messageController.messages[i].pos, this.size - 1)){
          check = true;
          this.pos.y += this.size / 2;
          this.delay = Math.min(this.delay + 10, messageController.messages[i].delay + 10)
        }
      }
      if(this.pos.y > height){
        this.pos.y = height;
        this.upCheck(false);
      }
    }
    messageController.messages.push(this);
  }
  upCheck(bump = true){
    if(bump)
      this.pos.y -= this.size / 2;
    for(var i in messageController.messages){
      if(i == messageController.messages.indexOf(this))
        continue;
      if(this.pos.equals(messageController.messages[i].pos, this.size - 1))
        messageController.messages[i].upCheck();
    }
  }
  
  update(ts, index){
    if(this.delay > 0){
      this.delay -= ts;
      return;
    }
    this.life -= ts
    if(this.life <= 0)
      messageController.messages.splice(index, 1);
  }
  draw(ctx){
    if(this.delay > 0)
      return;
    var size = this.size;
    if(this.life < 10)
      size = this.size * this.life / 10;
    else if(this.life > this._olife - 10)
      size = this.size * (1 - ((this.life - (this._olife - 10)) * .1));

    ctx.font = "bold " + size.toString() + "px sans-serif";
    ctx.textAlign = "center";
    ctx.fillStyle  = this.color;
    if(this.flashy)
      if(lastTime % 8 < 4)
        ctx.fillStyle = this.flashy;
    ctx.fillText(this.txt, this.pos.x, this.pos.y);
  }
}