﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {
    public PlayerController Owner;
    public Weapon weaponChild;
    public GameObject playerView;

    void Awake() {
        playerView = transform.parent.FindChild("PlayerView").gameObject;
        Weapon.wep_Pistol.setToController(this);
        //weaponChild.loadModel();
    }
    void Start() {
        if (!Owner) {
            if (transform.parent)
                Owner = transform.parent.gameObject.GetComponent<PlayerController>();
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (!Owner)
            return;
        if (weaponChild == null)
            return;

        if (Input.GetButton("Fire1"))
            weaponChild.trigger();
        if (Input.GetButton("Fire2"))
            weaponChild.altTrigger();
        if (Input.GetButton("Reload"))
            weaponChild.reload();

        weaponChild.update();
	}
    void LateUpdate() {
        weaponChild.handleAnimation();
    }

    public WeaponController setChild(Weapon child) {
        weaponChild = child;
        weaponChild.controllerParent = this;
        weaponChild.animObjectContainer = transform.GetChild(0).gameObject;
        weaponChild.animObject = weaponChild.animObjectContainer.transform.GetChild(0).gameObject;
        return this;
    }
    void equip(PlayerController owner) {
        Owner = owner;
    }
}

public class Weapon {
    public Weapon(WeaponController parent = null) {
        if (parent) {
            setToController(parent);
            muzzlePos = animObject.transform.localPosition + animObjectContainer.transform.localPosition;
            loadModel();
        }
    }

    public WeaponController controllerParent;
    public GameObject animObjectContainer;
    public GameObject animObject;
    public float timeVar;
    public wepType type;
    internal wepState state;


    public virtual Vector3 muzzlePosition {
        get { return muzzlePos; }
    }
    internal Vector3 muzzlePos;
    public virtual float stat_power {
        get { return stat_pow; }
    }
    internal float stat_pow;
    public virtual float stat_assault {
        get { return stat_ast; }
    }
    internal float stat_ast;
    public virtual float stat_range {
        get { return stat_rng; }
    }
    internal float stat_rng;
    public virtual float stat_difficulty {
        get { return stat_dif; }
    }
    internal float stat_dif;
    public virtual float stat_total {
        get {
            return (
                stat_power * 2 + 
                stat_assault + 
                stat_range + 
                (1 - stat_difficulty)) / 5;
        }
    }
    public virtual bool emptyClip {
        get { return false; }
    }

    public enum wepState {
        ready = 0,
        firing = 1,
        coolingDown = 2,
        reloading = 3,
        noAmmo = 4
    }
    public enum wepType {
        assault = 0,
        heavy = 1,
        sidearm = 2
    }

    public void setStats(float power, float assault, float range, float difficulty = 0.5f) {
        stat_pow = power;
        stat_ast = assault;
        stat_rng = range;
        stat_dif = difficulty;
    }

    public virtual void loadModel() {
        for (int i = animObject.transform.childCount - 1; i >= 0; i--)
            GameObject.Destroy(animObject.transform.GetChild(i).gameObject);

        GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        GameObject.Destroy(obj.GetComponent<BoxCollider>());
        obj.transform.localScale = new Vector3(0.1f, 0.1f, 2);
        obj.transform.parent = animObject.transform;
        obj.transform.localPosition = Vector3.zero;
    }
    public virtual void update() {
        switch (state) {
            case wepState.ready: update_ready(); break;
            case wepState.firing: update_firing(); break;
            case wepState.coolingDown: update_coolingDown(); break;
            case wepState.reloading: update_reloading(); break;
            case wepState.noAmmo: update_noAmmo(); break;
        }
    }
    public virtual void update_ready() {
        timeVar = 0;
    }
    public virtual void update_firing() { }
    public virtual void update_coolingDown() {
        this.timeVar -= Time.deltaTime;
        if (this.timeVar <= 0) {
            state = wepState.ready;
        }
    }
    public virtual void update_reloading() { }
    public virtual void update_noAmmo() { }
    public virtual void trigger() {
        if (state == wepState.reloading)
            return;
        if (emptyClip)
            reload();
    }
    public virtual void altTrigger() {
        if (state == wepState.reloading)
            return;
    }
    public virtual void reload() {
        if (state == wepState.reloading)
            return;
    }
    public virtual void handleAnimation() {
        animObjectContainer.transform.rotation = controllerParent.playerView.transform.rotation;
    }

    public Weapon setToController(WeaponController parent) {
        parent.setChild(this);
        return this;
    }

    public static Weapon Default{
        get {
            return new Weapon();
        }
    }
    public static Firearm wep_Pistol {
        get {
            Firearm f = new Firearm();
            f.setStats(0.1f, 0.2f, 0.5f);
            return f;
        }
    }
}

public class Firearm : Weapon {
    public Firearm() {
        setDefaults();
    }

    public Bullet proj;
    public float fireDelay;
    public float spread;
    public float projectileVelocity;
    public float reloadSpeed;
    public int projectileCount;
    public int clipSize;
    private int clipLeft;
    public override bool emptyClip {
        get {
            return clipLeft <= 0;
        }
    }

    void setDefaults() {
        proj = new Bullet();
        proj.damage = 1;
        proj.force = 1;
        fireDelay = 0.2f;
        spread = 1;
        projectileCount = 1;
        projectileVelocity = 10;
        reloadSpeed = 1;
        clipSize = 8;
        clipLeft = clipSize;
    }

    public override void update_reloading() {
        timeVar -= Time.deltaTime;
        if (timeVar <= 0) {
            clipLeft = clipSize;
            state = wepState.ready;
        }
    }
    public virtual void fire() {
        for(int i = projectileCount; i > 0; i--) {
            ProjectileController pcont = Projectile.spawn(proj, controllerParent.transform.GetChild(0).transform.GetChild(0).transform, controllerParent.Owner.gameObject);
            Debug.Log(controllerParent.transform.rotation);

            Vector3 vel = pcont.transform.rotation * Vector3.forward;
            vel *= projectileVelocity;
            pcont.GetComponent<Rigidbody>().velocity = vel;
        }
        clipLeft -= 1;

        state = wepState.coolingDown;
        timeVar = fireDelay;
    }

    public override void trigger() {
        if (state == wepState.ready)
            if (emptyClip)
                reload();
            else
                fire();    
    }
    public override void reload() {
        if (state == wepState.reloading)
            return;
        state = wepState.reloading;
        timeVar = reloadSpeed;
    }
}