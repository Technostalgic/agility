﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Look_Yaxis : MonoBehaviour {
    public float Sensitivity = 1;

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        float rotComp = Input.GetAxis("Mouse Y") * Sensitivity;
        Quaternion rotQuat = Quaternion.AngleAxis(rotComp, Vector3.left);
        
        this.transform.localRotation = this.transform.localRotation * rotQuat;

        //clamps the roatation so you can't look higher or lower than 90 degrees
        Vector3 eulerAngs = this.transform.localRotation.eulerAngles;
        if (eulerAngs.z >= 90)
            this.transform.localRotation = Quaternion.Euler(
                eulerAngs.x < 180 ? 90 : 270,
                0,
                0);
    }
}
