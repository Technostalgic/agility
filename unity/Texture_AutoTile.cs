﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Texture_AutoTile : MonoBehaviour {
    public float Correction = 1;
    Material material;

	// Use this for initialization
	void Start () {
        material = this.GetComponent<Renderer>().material;
        float maxscale = Correction * Mathf.Max(new float[] {
            transform.localScale.x,
            transform.localScale.y,
            transform.localScale.z });
        material.mainTextureScale = new Vector2(maxscale, maxscale);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
