﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour {
    public Projectile projectileChild;
    public bool disabled = false;

	// Use this for initialization
	void Start () {
        if (projectileChild == null)
            setChild(Projectile.Default);
	}
    public void Initialize() {
        projectileChild.initialize();
    }
	
	// Update is called once per frame
	void Update () {
        projectileChild.update();
	}

    void OnTriggerEnter(Collider other) {
        projectileChild.hit(other);
    }

    public void setChild(Projectile child) {
        projectileChild = child;
        projectileChild.controllerParent = this;
    }
}

public class Projectile {
    public Projectile() { }

    public ProjectileController controllerParent;
    public GameObject Owner;

    public static ProjectileController spawn(Projectile template, Transform orientation, GameObject owner = null) {
        ProjectileController p = GameObject.Instantiate<ProjectileController>(
            GameController.originalProjectileController, 
            orientation.position,
            orientation.rotation)
            as ProjectileController;

        p.setChild(template.copy());
        p.projectileChild.Owner = 
            owner != null ? 
                owner : orientation.gameObject;

        p.Initialize();
        return p;
    }

    public virtual void initialize() { }
    public virtual void update() { }
    public virtual void hit(Collider collision) {
        
    }

    public void setToController(ProjectileController controller) {
        controller.setChild(this);
    }

    public virtual Projectile copy() {
        Projectile p = new Projectile();
        p.copy_projectileFields(this);
        return p;
    }
    internal void copy_projectileFields(Projectile original) {
    }

    public static Projectile Default {
        get { return new Projectile(); }
    }
    public static Bullet proj_FirearmBullet{
        get {
            Bullet b = new Bullet();
            
            return b;
        }
    }
}

public class Bullet : Projectile {
    public Bullet() { }

    public float damage;
    public float force;

    public override void hit(Collider collision) {
        if (collision.gameObject.Equals(Owner))
            return;
        base.hit(collision);
    }
}