﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementHandle : MonoBehaviour {
    public float Speed = 10;
    public float MaxVelocityChange = 5;
    public float JumpPower = 1;
    public float Gravity = 10;
    bool grounded = false;
    Rigidbody body;

	// Use this for initialization
	void Start() {
        body = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate() {
        if (!grounded) {
            ApplyGravity();
            ApplyMovement(0.3f);
            return;
        }

        //ApplyGravity();
        if (Input.GetButton("Jump"))
            Jump();

        ApplyMovement(1);
        grounded = false;
	}

    private void OnCollisionStay(Collision collision) {
        foreach(ContactPoint contact in collision.contacts)
            if(contact.point.y < this.transform.transform.position.y - 0.9) {
                grounded = true;
                return;
            }
    }

    void ApplyMovement(float factor = 1) {

        Vector3 mDir = new Vector3();
        if (Input.GetButton("MoveForward"))
            mDir.z += 1;
        if (Input.GetButton("MoveBackward"))
            mDir.z -= 1;
        if (Input.GetButton("MoveRight"))
            mDir.x += 1;
        if (Input.GetButton("MoveLeft"))
            mDir.x -= 1;
        mDir.Normalize();
        mDir = this.transform.rotation * mDir;
        Vector3 tVel = mDir * Speed;

        Vector3 velocity = body.velocity;
        Vector3 velocityChange = (tVel - velocity);
        float mVelChange = MaxVelocityChange * factor;
        velocityChange.x = Mathf.Clamp(velocityChange.x, -mVelChange, mVelChange);
        velocityChange.z = Mathf.Clamp(velocityChange.z, -mVelChange, mVelChange);
        velocityChange.y = 0;
        body.AddForce(velocityChange, ForceMode.VelocityChange);

    }
    void Jump() {
        body.AddForce(new Vector3(0, JumpPower * body.mass, 0), ForceMode.Impulse);
    }
    void ApplyGravity() {
        body.AddForce(new Vector3(0, -Gravity * body.mass, 0));
    }
}
