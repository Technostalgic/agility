﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Look_Xaxis : MonoBehaviour {
    public float Sensitivity = 1;
    //public Quaternion originalRot;

	// Use this for initialization
	void Start () {
        //originalRot = this.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
        float rotComp = Input.GetAxis("Mouse X") * Sensitivity;
        Quaternion rotQuat = Quaternion.AngleAxis(rotComp, Vector3.up);

        this.transform.localRotation = this.transform.localRotation * rotQuat;
	}
}
